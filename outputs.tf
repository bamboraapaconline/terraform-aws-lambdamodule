output "lambda_role_id" {
  value = "${aws_iam_role.role.id}"
  description = "Lambda assigned role id"
}

output "lambda_arn" {
  value = "${aws_lambda_function.lambda.arn}"
  description = "Lambda arn"
}

output "lambda_version" {
  value = "${aws_lambda_function.lambda.version}"
  description = "Lambda published version"
}

output "alias_arn" {
  value = "${aws_lambda_alias.lambda_alias.arn}"
  description = "Lambda asias arn"
}
