# AWS Lambda function
resource "aws_lambda_function" "lambda" {
  function_name     = "${var.function_name}"
  role              = "${aws_iam_role.role.arn}"
  handler           = "${var.function_handler}"
  runtime           = "${var.function_runtime}"
  timeout           = "${var.timeout}"
  memory_size       = "${var.memory_size}"
  s3_bucket         = "${var.lambda_bucket}"
  s3_key            = "${local.s3_key}"
  s3_object_version = "${data.aws_s3_bucket_object.lambda_script.version_id}"
  description       = "${data.aws_s3_bucket_object.lambda_script.tags["Version"]}"

  tags {
    Region      = "${var.region}"
    Environment = "${terraform.workspace}"
    Project     = "${var.billing_tag}"
    CreatedBy   = "${var.created_by_tag}"
  }

  kms_key_arn = "${var.kms_key_arn}"

  tracing_config {
    mode = "Active"
  }
}

data "aws_s3_bucket_object" "lambda_script" {
  bucket     = "${var.lambda_bucket}"
  key        = "${local.s3_key}"
  version_id = "${var.s3_file_version}"
}

locals {
  s3_key = "${var.application}/${terraform.workspace}_${var.function_name}.zip"
}
