# Lamda Alias
resource "aws_lambda_alias" "lambda_alias" {
  name             = "${terraform.workspace}"
  function_name    = "${aws_lambda_function.lambda.function_name}"
  function_version = "${aws_lambda_function.lambda.version}"
}
