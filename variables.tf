variable "function_name" {
  description = "lambda function name"
}

variable "function_handler" {
  description = "lambda function handler"
}

variable "function_runtime" {
  description = "lambda function runtime"
  default = "dotnetcore2.0"
}

variable "lambda_bucket" {
  description = "S3 bucket that contains lambda source code zip file"
  default     = "bambora-lambda-code-container-dev"
}

variable "s3_file_version" {
  description = "S3 zip file version"
  default     = "$LATEST"
}

variable "region" {
  description = "The region that lambda will be created in"
  default = "ap-southeast-2"
}

variable "billing_tag" {
  description = "This could be use for billing purpose"
}

variable "created_by_tag" {
  description = "To identify the creator"
  default = "Terraform"
}

variable "kms_key_arn" {
  description = "kms key for environment variable value decrytion"
}

variable "application" {
  description = "Application/Product name"
}

variable "codedeploy_role_arn" {
  description = "role arn that has arn:aws:iam::aws:policy/service-role/AWSCodeDeployRoleForLambda as policy"
}

variable "memory_size" {
  default = "2048"
  description = "need to optimised base on execution time vs response time (default:2048)"
}

variable "timeout" {
  default = "30"
  description = "need to set based on execution time (default:30s)"
}


variable "deployment_config" {
  description = "deployment config name"
  default= "CodeDeployDefault.LambdaCanary10Percent30Minutes"
}
