resource "aws_codedeploy_deployment_group" "lambda_deployment" {
  app_name              = "${var.application}"
  deployment_group_name = "${var.function_name}"
  service_role_arn      = "${var.codedeploy_role_arn}"
  deployment_config_name = "${var.deployment_config}"

  deployment_style {
    deployment_option = "WITH_TRAFFIC_CONTROL"
    deployment_type   = "BLUE_GREEN"
  }
}