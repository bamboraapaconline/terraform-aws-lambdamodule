#Lambda Cloudwatch Policy
resource "aws_iam_role_policy" "lambda_cloudwatch_policy" {
  name = "Lambda_Cloudwatch_Policy"
  role = "${aws_iam_role.role.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:DescribeLogGroups",
                "logs:DescribeLogStreams",
                "logs:PutLogEvents"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

#Lambda xray policy
resource "aws_iam_role_policy" "lambda_xray_policy" {
  name = "Lambda_XRay_Policy"
  role = "${aws_iam_role.role.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "xray:PutTelemetryRecords",
                "xray:PutTraceSegments"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}


resource "aws_iam_role_policy" "lambda_kms_policy" {
  name = "Lambda_KMS_Policy"
  role = "${aws_iam_role.role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action" : [
                "kms:Decrypt"
            ],
            "Resource": "${var.kms_key_arn}"
        }
    ]
}
EOF
}